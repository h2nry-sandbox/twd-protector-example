package com.h2nry.twdp.twdpapi;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.h2nry.twdp.twdp.TwdpDllBridge;

import lombok.RequiredArgsConstructor;


@RestController
@RequiredArgsConstructor
public class TestController {
    private final TwdpDllBridge bridge;

    @GetMapping("/getProductString")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public ResponseEntity<Void> getProductString() {
        long dwDeviceNum = 1234;
        String lpvDeviceString = "1234";
        long dwFlags = 1;

        bridge.getProductString(dwDeviceNum, lpvDeviceString, dwFlags);

        return ResponseEntity.noContent().build();
    }

    @GetMapping("/open")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public ResponseEntity<Void> open() {
        long dwDeviceNum = 1234L;
        long handle = 1234L;

        bridge.open(dwDeviceNum, handle);

        return ResponseEntity.noContent().build();
    }

    @GetMapping("/open")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public ResponseEntity<Void> close() {
        long handle = 1234L;

        bridge.close(handle);

        return ResponseEntity.noContent().build();
    }
}

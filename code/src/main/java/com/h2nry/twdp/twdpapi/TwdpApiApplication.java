package com.h2nry.twdp.twdpapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TwdpApiApplication {
	public static void main(String[] args) {
		SpringApplication.run(TwdpApiApplication.class, args);
	}
}
